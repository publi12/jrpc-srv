const { hashify_rpc } = require("./libs/rpc_flatter");
const { build_doc } = require("./libs/doc_extractor");
const { build_rpc_client } = require("./libs/rpc_client");
const { Router } = require("express");

/**
 * @typedef {Object} JRun Runner for rpc infrastructure
 * @property { {funcs:[], types:[]} } doc Documentation of rpc
 * @property {*} hash Hash for calling functions {name: caller_func}
 */


/**
 * @typedef {Object} JRouterOptions Runner for rpc infrastructure
 * @property { bool } logging Enables output erros in console
 */
 
/**
 * Creates structure to make rpc calls.
 * @param {*} rpc Object containing all nested rpc functions
 * @returns {JRun}
 */
function build_rpc_runner(rpc) {
	const hash = hashify_rpc(rpc);
	const doc = build_doc(rpc);

	return { hash, doc};
}

/**
 * Creater router for calling RPC (/docs, /call)
 * @param { JRun } rpc_runner
 * @param { JRouterOptions } opts
 * @returns {Router}
 */
function build_rpc_router(rpc_runner, opts = {}) {
	const router = Router();

	router.use((req, res, next) => {
		req.rpc = rpc_runner
		// req.logging = opts.logging
		req.logging = true
		next();
	});

	router.use(require("./rpc_routes"));

	return router;
}

module.exports = {
	build_rpc_runner,
	build_rpc_router,
	build_rpc_client,
};
