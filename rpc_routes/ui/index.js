const api_url = "..";

function parseValue(v){
	try{
		return JSON.parse(c.value)
	}
	catch {
		return v
	}	
}

const api = {
	docs: [1],

	reload() {
		fetch(`${api_url}/docs/funcs`)
			.then((x) => x.json())
			.then((x) => {
				this.docs = x.map((y) => ({ ...y, visible: false }));
				m.redraw();
			});
	},

	/**
	 * Call some function (params gets from doc)
	 * @param {*} doc
	 */
	async call(doc) {
		const headers = {
			"Content-Type": "application/json;charset=UTF-8",
		};

		const params = doc.params ? doc.params.reduce((s, c) => ({ ...s, [c.name]: parseValue(c.value) }), {}) : null;
		const body = { method: doc.name, params };

		doc.response = "Sending...";

		try {
			const res = await fetch(`${api_url}/call`, { method: "POST", headers, body: JSON.stringify(body) });
			if (!res.ok) {
				doc.response = {
					status: "Network error",
					code: res.status,
				};
			} else {
				const json = await res.json();
				doc.response = json;
			}
			m.redraw();
		} catch (ex) {
			console.log(ex);
			doc.response = {
				status: "Sending error",
				code: (ex && ex.message) || ex,
			};			
		}
	},

	ui: {
		collapse_swap(doc) {
			doc.visible = !doc.visible;
		},
	},
};

let App = {
	view(node) {
		return m(
			"div",
			api.docs.map((x) => m(doc_block, { doc: x }))
		);
	},
};

const docs = {
	api,
	view(node) {
		return m("div", node.state.api.x);
	},
};

// Api item card
const doc_block = {
	submit() {
		api.call(this.attrs.doc);
	},

	view(node) {
		const doc = node.attrs.doc;
		const title = m(doc_title, { doc: node.attrs.doc });
		const body = m(doc_body, { doc: node.attrs.doc });
		// const test = m(test_block, { doc: node.attrs.doc, submit: node.tag.submit.bind(node) });

		// const result = m(block_result, { doc });

		// const wrapper = m("div.block.frame.card", {}, [title, params, test, result]);
		const wrapper = m("div.block.frame.card", {}, [title, body]);

		return [wrapper];
	},
};

// const svg_icon = {
// 	view(node) {
// 		return m('svg')
// 	}
// }

// Api item header
const doc_title = {
	view(node) {
		const doc = node.attrs.doc;
		const icon = m(
			"span.frame",
			{
				style:{padding: '2px 4px', cursor: 'pointer', 'margin-right': '8px', 'font-family':'monospace'},
				onclick() {
					api.ui.collapse_swap(doc);
				},
			},
			doc.visible ? "-" : "+"
		);
		const title = m("b.card-title", doc.name);
		const desc = m("span", doc.desc);

		// const params = doc.params && doc.params.map((param) => m(param_block, { param }));

		const block = m("div.block", [icon, title, desc]);

		return [block];
	},
};

// Api item body
const doc_body = {
	view(node) {
		const doc = node.attrs.doc;
		const params = m(doc_params, { doc: node.attrs.doc });
		const test = m(test_block, { doc: node.attrs.doc });

		const result = m(block_result, { doc });
		// console.log(node.tag)
		const wrapper = m("div.doc-body", { style: { display: doc.visible ? "block" : "none" } }, [params, test, result]);

		return [wrapper];

		// const params_header = m("div",{style: {display: 'none2'}}, 'asdasdas');
		// return params_header;
	},
};

// Api item header
const doc_params = {
	view(node) {
		const doc = node.attrs.doc;

		const params = doc.params && doc.params.map((param) => m(param_block, { param }));

		return [params];
	},
};

const param_block = {
	view(node) {
		const param = node.attrs.param;
		const name = m("b.param-name", `${param.name}`);
		const type = m("span.type", `${param.type}`);

		const input = m("input", {
			style: { display: "block", width: "100%" },
			oninput(e) {
				param.value = e.target.value;
			},
		});

		const desc = m("div.param-desc", `${param.desc}`);

		const block = m("div.block", [name, type, input, desc]);

		return block;
	},
};

const test_block = {
	view(node) {
		// const { submit } = node.attrs;

		return m(
			"button.btn-primary",
			{
				onclick(e) {
					api.call(node.attrs.doc);
				},
			},
			"submit"
		);
	},
};

const block_result = {
	view(node) {
		const { response } = node.attrs.doc;

		if (response) {
			return m("div.block.code", JSON.stringify(response, null, 2));
		}
	},
};

m.mount(document.getElementById("mithril-ui"), App);

api.reload();
