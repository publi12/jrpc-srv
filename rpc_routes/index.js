const express = require("express");

const router = require("express").Router();

router.use(require("./_handler"));

// router.get("/docs", (req, res) => {
// 	res.send(req.doc.map(x => ({name:x.name, ...x.doc, name:x.name})));
// });

router.get("/docs/funcs", (req, res) => {
	res.send(req.rpc.doc.funcs.map((x) => ({ name: x.name, ...x.doc, name: x.name })));
});

router.get("/docs/types", (req, res) => {
	res.send(req.rpc.doc.types);
});

router.use('/ui', express.static(`${__dirname}/ui`))

router.get("/docs/funcs/:name", (req, res) => {
	res.send(
		req.rpc.doc.funcs
			.filter((x) => x.name.startsWith(req.params.name))
			.map((x) => ({ name: x.name, ...x.doc, name: x.name }))
	);
});

function make_call(rpc, { id, method, params }) {
	if (!method) throw `Field method not defined`;

	const func = rpc.hash[method];
	if (!func) throw `RPC method ${method} not found`;

	if (!params) params = [];
	else if (!Array.isArray(params)) {
		const doc = rpc.doc.funcs.filter((x) => x.name == method)[0];
		if (!doc || !doc.doc || !doc.doc.params) throw `Method ${method} not documented. Can not correlate params.`;

		params = doc.doc.params.map((x) => params[x.name] || null);
	}

	const call = new Promise(async (resolve, reject) => {
		try {
			resolve(await func(...params));
		} catch (ex) {
			reject(ex);
		}
	});
	return { id, call };
}

router.post("/call", (req, res) => {
	try {
		const { call, id } = make_call(req.rpc, req.body);
		res.handle(call, id);
	} catch (ex) {
		res.error(ex);
	}
});

router.post("/rawcall", async (req, res) => {
	try {
		const { call } = make_call(req.rpc, req.body);
		await call.then((x) => res.status(200).send(x));
	} catch (ex) {
		res.status(500).send(ex.message || ex);
	}
});

module.exports = router;
