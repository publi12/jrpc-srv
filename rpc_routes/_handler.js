const express = require("express")
const router = express.Router();

router.use(express.json({limit: '10mb'}))

router.use((req, res, next) => {
	res.ok = (data, id) => res.send({ id, success: true, data });
	res.error = (ex, id) => res.send({ id, success: false, message: ex && ex.message || ex });
	res.handle = (promise, id) => {		
		promise
			.then((r) => res.ok(r, id))
			.catch((ex) => {
				let msg = ex && ((ex.response && ex.response.data) || ex.message || ex);
				if (req.logging)
					console.error(`Handled request fail\r\n- ${msg} -\r\n`, req.query, req.body, ex && ex.stack || ex);
				res.error(msg, id);
			});
	};
	next();
});

module.exports = router