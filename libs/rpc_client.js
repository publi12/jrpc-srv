const axios = require("axios");

function build_rpc_caller(rpc_call_info, rpc) {
	return function (...params) {
		return {
			call() {
				return rpc.call(rpc_call_info.name, params);
			},

			push() {
				return rpc.push(rpc_call_info.name, params);
			},
		};
	};
}

async function build_rpc_client(url, opts) {
	opts = opts || {};
	function handler(req) {
		return req
			.then((x) => {
				if (x.data.success) return x.data.data;
				throw x.data.message;
			})
			.catch((ex) => {
                console.log(ex)
				throw ex.message || ex;
			});
	}

	function get(sub_url) {
		return handler(axios.get(`${url}${sub_url}`));
	}

	function post(sub_url, body) {
		return handler(axios.post(`${url}${sub_url}`, body, { headers: opts.headers || {} }));
	}

	const rpc = {
		url,
		headers: opts.headers || {},

		async call(method, params) {
			return post("/call", { method, params });
		},

		get_doc(name) {
			return this.docs.filter((x) => x.name == name)[0];
		},
	};
	rpc.doc = { funcs: await axios.get(`${url}/docs/funcs`).then((x) => x.data) };

	rpc.methods = {};
	rpc.doc.funcs.forEach((x) => {
		const names = x.name.split(".");
		const lastname = names.pop();
		const last = names.reduce((s, c) => (s[c] = s[c] || {}), rpc.methods);
		last[lastname] = build_rpc_caller(x, rpc);
	});

	return rpc;
}

module.exports = {
	build_rpc_client,
};
