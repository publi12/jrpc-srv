function flatify_rpc(rpc, prefix = "") {
	const keys = Object.keys(rpc).filter(x => !x.startsWith('_'));
	const k_funcs = keys.filter((x) => typeof rpc[x] == "function" && (!rpc[x].prototype || Object.getOwnPropertyDescriptor(rpc[x], 'prototype').writable))
	const k_rpcs = keys.filter((x) => typeof rpc[x] == "object");

	prefix += prefix ? "." : "";

	const rpc_funs = k_rpcs.reduce((s, c) => [...s, ...flatify_rpc(rpc[c], prefix + c)], []);
	const hash_funs = k_funcs.map((x) => ({ flat_name: prefix + x, func: rpc[x], rpc }));

	return [...hash_funs, ...rpc_funs];
}

function hashify_rpc(rpc) {
	const flat_rpc = flatify_rpc(rpc)
	return flat_rpc.reduce((s, c) => ({...s, [c.flat_name]: c.func.bind(c.rpc)}) ,{})
}


module.exports = {
	flatify_rpc,
	hashify_rpc
};
