const fs = require('fs')

/**
 * Grab section balanced with left and right tokens
 * @param {*} text
 * @param {*} pos
 * @param {*} lb
 * @param {*} rb
 * @returns Text into biggest balanced group as [start, end] or [0,0].
 */
function grab_balanced(text, pos = 0, lb = "{", rb = "}") {
	let ans = [0, 0];
	let depth = 0;
	for (let i = pos; i < text.length; i++) {
		if (text[i] == lb) {
			if (!depth) ans[0] = i;
			depth++;
		} else if (text[i] == rb) {
			depth--;
			if (!depth) {
                ans[1] = i;
                return ans
            }
		}
	}
	return ans;
}

function parse_param(line){
    const fl = line.match(/@\w+[^\w{]+(.)/)
    const icb = (fl[1] == '{') ? grab_balanced(line) : [0,0];
    const type = icb[1] ? line.substring(icb[0] + 1, icb[1]).trim() : '*';
    const text = line.substring(icb[1] + 1).trim();
    const pos = text.indexOf(" ")
    let name = pos > 0 ? text.substring(0, pos) : text;
    const desc = (pos > 0 ? text.substring(text.indexOf(" ")) : '').trim();

    let optional = false
    let def = null
    if (name.startsWith('[')){
        name = name.replace('[', '').replace(']', '')
        optional = true
        const pos2 = name.indexOf('=')
        if (pos2 > 0){
            def = name.substring(pos2 + 1).trim()
            name = name.substring(0, pos2)
        }        
        name = name.trim()
    }

    let ret = {name, type, desc}
    if (optional) ret = {...ret, optional}
    if (def) ret = {...ret, def}

    return ret
}

/**
 * Extract jsdoc comments from js file
 * @param {*} text
 * @returns { {name, desc, params, examples, returns} }
 */
function extract_comment(text) {
	cmt = text.split("\n");

	let state = "desc";
	doc = { desc: "" };

	for (let i = 0; i < cmt.length; i++) {
		const line = cmt[i].replace('* ', '').trim();

		if (line.startsWith("@")) {
			state = line.substring(1, line.indexOf(" "));

			if (state == "param") {
                const p = parse_param(line)
				doc.params =[...doc.params || [], p];
			} else if (state == "example") {
				const desc = line.substring(line.indexOf(" ")).trim();
                doc.examples =[...doc.examples || [], desc];
			} else if (state == "returns") {
                const p = parse_param(line)
				doc.returns = {type: p.type, desc: [p.name == 'returns' ? '' : p.name, p.desc].join(' ').trim()}
			} else if (state == "typedef") {
                const p = parse_param(line)
                doc.name = p.name
                doc.desc = p.desc
                doc.type = p.type
				doc.typedef = true
			} else if (state == "property") {
                const p = parse_param(line)
				doc.properties =[...doc.properties || [], p];
			}
            
			// doc[state] = line.substring(line.indexOf(' '))
		} else {
			if (state == "desc") doc.desc += " " + line;
			else if (state == "returns") doc.returns.desc += " " + line;
			else if (state == "param") doc.params[doc.params.length - 1].desc += " " + line;
			else if (state == "example") doc.examples[doc.examples.length - 1] += " " + line;
            else if (state == "typedef") doc.typedef += " " + line;
            else if (state == "property") doc.properties[doc.properties.length - 1].desc += " " + line;
		}
	}

	doc.desc = doc.desc.trim();

	return doc;
}

/**
 * Extract all jsdoc comments from js file
 * @param {*} text
 * @returns { [{name, desc, params, examples, returns}] }
 */
function extract_comments(text) {
	const re = RegExp(/\/\*\*(.+?)\W\*\//gms);

	let res = [...text.matchAll(re)];

    return res.map(x => {
        const doc = extract_comment(x[1]);
        const index2 = x.index + x[0].length;
        const index3 = text.indexOf("(", index2)
        const index4 = text.indexOf("/**", index2)

        if (index4 < 0 || (index4 > 0 && index3 < index4))
            doc.name = text.substring(index2, index3).split(" ").slice(-1)[0];

        return doc
    })
}

function hashefy_cache(cache){
    return Object.keys(cache.exports).reduce((s, c) => [...s, {func: cache.exports[c], name: c, file: cache.id}], []).filter(x => typeof x.func == 'function')
}

function build_cache_hash(){
    const keys = Object.keys(require.cache).filter(x => !x.includes('node_modules'))
    return keys.reduce((s, c) => [...s, ...hashefy_cache(require.cache[c])], [])    
}

/**
 * Build documentation for RPC module.
 * @param {*} rpc root rpc module
 * @returns 
 */
function build_doc(rpc){
    const { flatify_rpc } = require('./rpc_flatter')
    
    let flat_rpc = flatify_rpc(rpc)

    const ch = build_cache_hash()

    flat_rpc.forEach(x => x.files = [...x.files || [], ...ch.filter(y => x.func == y.func).map(y => y.file)])

    // Join files from cache to flat_rpc
    let rpc_with_files = flat_rpc.map(x => {
        const h = ch.filter(y => y.func == x.func && y.func.name == y.name)[0] || {}
        return {...h, ...x}
    })

    const files = Array.from(new Set(rpc_with_files.reduce((s, c) => [...s, ...c.files], []))) //[...rpc_with_files.reduce((s, c) => s.add(...c.files), new Set())].filter(x => x)

    // And build docs
    const parsed_comments = files.reduce((s,c) => ({...s, [c]:extract_comments(fs.readFileSync(c).toString()) }) , {})
    
    // Attach doc section from parsed comments (by filename + func_name)
    rpc_with_files.forEach(x => {
        x.doc = x.files.map(y => parsed_comments[y]).reduce((s, c) => [...s, ...c], []).filter(y => y.name == x.name)[0]
        x.func = x.func.bind(x.rpc)
    });

    const types = Object.values(parsed_comments).reduce((s,c) => [...s, ...c], []).filter(x => x.typedef)

    return {
        funcs: rpc_with_files.map(x => ({name: x.flat_name, func: x.func, doc: x.doc})), 
        types: types
    }
}

module.exports = {
	extract_comment,
    extract_comments,
    build_doc
};
